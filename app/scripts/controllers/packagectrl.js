'use strict';

/**
 * @ngdoc function
 * @name yapp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of yapp
 */
angular.module('yapp')
  .controller('OverviewCtrl', function($scope,$q,ngDialog) {
    $scope.list1 = {'title': 'Apple','img':'http://www.kimmelorchard.org/img/icon_apple_gala.png'};
    $scope.list3 = {'title': 'Orange','img':'http://icons.iconarchive.com/icons/bingxueling/fruit-vegetables/256/orange-icon.png'};
    $scope.list4 = {'title': 'XYZ','img':'https://dtgxwmigmg3gc.cloudfront.net/files/53d8507ae1272f5c6900008a-icon-256x256.png'};

    $scope.list2 = [];

    //script for custom package

    $scope.custom_package = [{
      'name':'Jhon Doe',
      'user_id' : 'ID300495'
    }];


    $scope.beforeDrop = function() {

      var deferred = $q.defer();

      deferred.resolve();

      ngDialog.open({ 
        template: 'views/popups/pop.html', 
        className: 'ngdialog-theme-default',
        scope: $scope 
      }).closePromise.then(function (data) {
          console.log(data.id + ' has been dismissed.');
      });

      return deferred.promise;
   
    };

    $scope.reset = function() {
          alert("Test");
    };


  });
